const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");

const exercisesRouter = require("./routes/exercises");
const usersRouter = require("./routes/users");

// Use dotenv instead of system variables
require("dotenv").config();

//create express server
const app = express();
const port = process.env.PORT || 5000;

// Middleware
app.use(cors());
app.use(express.json()); // parse JSON

// connect to Mongo Atlas
const uri = process.env.ATLAS_URI;
mongoose
  .connect(uri, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Successfully connected!"))
  .catch(e => console.log(JSON.stringify(e)));

// Set up endpoints
app.use("/exercises", exercisesRouter);
app.use("/users", usersRouter);

// start server
app.listen(port, () => {
  console.log(`Server's up and running on port: ${port}`);
});
