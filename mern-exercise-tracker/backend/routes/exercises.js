const router = require("express").Router();
let Exercise = require("../models/exercise.model");

// GET Route
router.route("/").get((req, res) => {
  Exercise.find()
    .then(exercises => res.json(exercises))
    .catch(err => res.status(400).json(`Error: ${err}`));
});

// GET by id
router.route("/:id").get((req, res) => {
  Exercise.findById(req.params.id)
    .then(exercise => res.json(exercise))
    .catch(err => res.sendStatus(404).json(`Error! User not found: ${err}`));
});

// POST route
router.route("/add").post((req, res) => {
  const username = req.body.username;
  const description = req.body.description;
  const duration = Number(req.body.duration);
  const date = Date.parse(req.body.date);

  const newExercise = new Exercise({
    username,
    description,
    duration,
    date,
  });

  newExercise
    .save()
    .then(() => res.json("Exercise successfully added!"))
    .catch(err => res.sendStatus(400).json(`Error: ${err}`));
});

// UPDATE route by id
router.route("/update/:id").post((req, res) => {
  Exercise.findById(req.params.id)
    .then(exercise => {
      exercise.username = req.body.username;
      exercise.description = req.body.description;
      exercise.duration = Number(req.body.duration);
      exercise.date = Date.parse(req.body.date);

      exercise
        .save()
        .then(() => res.json("Exercise saved!"))
        .catch(err =>
          res.sendStatus(400).json(`Something went wrong while updating ${err}`)
        );
    })
    .catch(err => res.sendStatus(400).json(`Error updating exercise: ${err}`));
});

// DELETE by id
router.route("/:id").delete((req, res) => {
  Exercise.findByIdAndDelete(req.params.id)
    .then(() => res.json("Exercise deleted successfully."))
    .catch(err => res.json(`Error: ${err}`));
});

module.exports = router;
