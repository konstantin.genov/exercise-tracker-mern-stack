# Exercise Tracker - MERN STACK

An exercise tracker application built by using the MERN stack. (MongoDB, Express, React, Node.js and Mongoose)

The database is hosted on MongoDB [Atlas] - (https://www.mongodb.com/cloud/atlas)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


Change directory to backend to run:
### `nodemon server`

This will start the Express server locally. 

In order to establish a mongoose connection you need to add a system variable or use .env file with the connection URI.